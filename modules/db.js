/**
 * Created by ss0088 on 06/07/2017.
 */
var mysql = require('mysql');

var dev = true;

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : dev ? 'root' : 'root',
    password : dev ? 'shakalaka' : 'senzations2k17',
    database : 'wine-iot'
});

connection.connect();

module.exports = connection;