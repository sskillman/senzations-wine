/**
 * Created by ss0088 on 06/07/2017.
 */

var axios = require('axios');
var db = require('./db');
var OP_KEY = "fZ8nBAIRsvwkx0xmCQ9vrFEbVkTd0e4dvYFhREU5KUhhI3nqxDjJRF3ad8LvBru5bWFoi4v8Kt519KpE";
var url = "https://api.evrythng.com";
var path = "/thngs";
var moment = require('moment');
var interval;
var async = require('async');

// rate limit is 5000 requests per day
// approx 20s between request
function startPolling(){
    getData(afterPolling);
    interval = setInterval(getData, 25000,afterPolling);
}

function stopPolling() {
    clearInterval(interval);
}

function afterDataInsert (err, thng) {
    if(err){
        var erro = 'Warn: EUI='+thng.identifiers.deviceEUI+' Message=';
        if(err.code === 'ER_DUP_ENTRY'){
            erro +='Old data, not inserting.';
        }else {
            erro += err.message;
        }
        return console.warn(erro);
    }

    console.log('OK: EUI='+thng.identifiers.deviceEUI+' Message=New data inserted.');
}

function afterPolling(err) {
    if(err){
        return console.error(err.message);
    }
    console.log('Polling finished without errors.');
}

function getData(cb) {
    console.log('Polling Evrythng.');
    axios.request({
        method: 'get',
        url: url + path,
        headers: {
            "Authorization": OP_KEY,
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Cache-Control": "no-cache"
        }
    }).then(function (resp) {
        var data = resp.data;
        async.each(data,
            function (thng, callback) {

                if(!thng.properties.sensor || !thng.updatedAt){
                    var e = new Error('Missing required data.');
                    afterDataInsert(e, thng);
                    return callback();
                }
                query = 'INSERT INTO `sensor-data` SET ?';
                data = {
                    time: moment(thng.updatedAt).format('YYYY-MM-DD HH:mm:ss'),
                    vineyard_id: thng.properties.location,
                    sensor_id: thng.properties.sensor,
                    temp_soil: thng.properties.temperature2,
                    temp_air: thng.properties.temperature,
                    long: thng.properties.longitude,
                    lat: thng.properties.latitude,
                    light: thng.properties.light
                };

                return db.query(query, data, function (err, a) {
                    afterDataInsert(err, thng);
                    callback();
                });
            },
            cb);
    }).catch(cb);
}


module.exports = {
    startPolling: startPolling,
    stopPolling: stopPolling
};