/**
 * Created by ss0088 on 06/07/2017.
 */

var db = require('../modules/db');

module.exports = function (req, res, next) {
    query = 'SELECT * FROM `sensor-data`';

    db.query(query, function (err, results, fields) {
        if (err) {
            return next(err);
        }

        res.json(results);
    });
};