var express = require('express');
var path = require('path');
var logger = require('morgan');
var getAllData = require('../middleware/getAllSensorData');
var app = express();

// view engine setup

app.use(logger('dev'));

app.use('/senzations-wine', getAllData);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.sendStatus(err.status || 500);
});

module.exports = app;
